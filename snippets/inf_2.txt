$module = 'loads'; // переменная отвечающая за модуль от куда брать материал
$limit = 3; // переменная отвечающая за количество материалов (рекомендованные значения 3; 6 ;9 ; 12 ;15 )



$Register = Register::getInstance();

$Cache = $Register['Cache'];

$key = 'files_inf';
if ($this->Cache->check($key)) {
$load = json_decode($this->Cache->read($key), true);
} else {
$load = $Register['DB']->select($module, DB_ALL, array('order' => 'date DESC', 'limit' => $limit));
 $this->Cache->write(json_encode($load), $key, array(), array('lifetime' => 900));
}


if(count($load) > 0) {
foreach ($load as $files) {
$url_full = get_url(WWW_ROOT . $module .'/view/'. $files['id']);
$announce = $Register['PrintText']->getAnnounce($files['main'], $url_full, 0, 200);
$image = $Register['DB']->select($module.'_attaches', DB_ALL, array('cond' => array('entity_id' => $files['id'])));
if (count($image) > 0) {
$img = '<img src="'.get_url('/image/'.$module.'/full/'.$image[0]['filename']).'" class="ph-t" />';
} else {
$img = '<img src="'.get_url(getTemplateName().'/image/noimage.png').'" class="ph-t" />';
}

echo '<div class="ph-b"> <div class="ph-ttl"><a href="'. $url_full .'" title="'. $files['title'].'">'.$files['title'] .'</a></div> <div class="ph-i"> <div class="ph-img">'. $img .'<a href="'. $url_full .'"><span></span></a> <a href="'. $url_full .'" class="ph-lnk"><span></span></a> </div> </div> <div class="post-m"> <div class="post-l"> <span class="m-date" title="Дата">'. $files['date'] .'</span> <span class="m-read" title="Просмотры">'. $files['views'] .'</span> <span class="m-load" title="Загрузки">'. $files['downloads'] .'</span> <div class="clr"></div> </div> </div> <div class="post-d"> <div class="post-f">'. $announce .'</div> </div> <div class="post-mr"><a href="'. $url_full .'">Подробнее ></a></div> </div>';
}
}
