$module = 'news'; // переменная отвечающая за модуль от куда брать материалы(если нужно поменяйте loads - файлы ;  stat- статьи)
$limit = 7; // количество материалов выводимых в слайдере
// дальше без минимальных знаний в php не лезть!!!



?>
<section>
<div id="slider-b">
<div id="slider" class="flexslider no-js">
<ul class="slides">
<?
$slider_news = $Register['DB']->select($module, DB_ALL, array('order' => 'date DESC', 'limit' => $limit));
if (count($slider_news) > 0) {
foreach ($slider_news as $slider) {
$url_full = get_url(WWW_ROOT . $module .'/view/'. $slider['id']);
$announce = $Register['PrintText']->getAnnounce($slider['main'], $url_full, 0, 200);



$image = $Register['DB']->select($module.'_attaches', DB_ALL, array('cond' => array('entity_id' => $slider['id'])));
if (count($image) > 0) {
    $img = '<img src="'.get_url('/image/'.$module.'/full/'.$image[0]['filename']).'" />';
} else {
    $img = '<img src="'.get_url(getTemplateName().'/image/noimage.png').'" />';
}

echo '<li class="slide"> <a href="'.WWW_ROOT .$module .'/view/'.$slider['id'].'">'. $img .'</a> <div class="caption"> <div class="slide-dsc"> <div class="slide-ttl"><a href="'.WWW_ROOT .$module .'/view/'.$slider['id'].'">'.$slider['title'].'</a></div> <div class="slide-txt">'. $announce  .'</div> </div> </div> </li>';
}
}
?>

</ul>
</div>
<script type="text/javascript">
$(document).ready(function(){
$("#slider.flexslider").flexslider({
slideshowSpeed: 5000,
animationSpeed: 800,
before: function(){ 
$('.caption').animate({opacity:0},300);
},
after: function(){
$('.caption').animate({opacity:1},300);
}
});
});</script>
</div>
</section>
<hr class="ph-hr">

<?