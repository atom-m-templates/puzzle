// Запуск FancyBox
$(document).ready(function() {
	$("a.gallery").fancybox();
});



function replyComment(el, id, name){
    $('.comment_tree').removeClass('reply_comment');
    $(el).parents('.comment_tree').addClass('reply_comment');
    $('.addcomment .inftitle').html('Ответ для '+name);
    $('.addcomment input[name=reply]').val(id);
    $('.addcomment .canselReplyComment').css('display', 'inline-block');
}

function canselReplyComment(){
    $('.comment_tree').removeClass('reply_comment');
    $('.addcomment .inftitle').html('Добавление комментария');
    $('.addcomment input[name=reply]').val(0);
    $('.addcomment .canselReplyComment').css('display', 'none');
}